package edu.pucmm;

import edu.pucmm.domains.Student;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.List;

/**
 * Documentación en http://www.vogella.com/tutorials/AndroidTesting/article.html
 * <p>
 * Created by aluis on 8/19/15.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ParcialTest {

    @Test
    public void studentsTest() {
        List<Student> students = Parcial.get().getStudents();
        for (Student student : students) {
            System.out.println(student.toString());
        }
        System.out.println("To Count: "  + Parcial.get().getStudentsCount());
        System.out.println("To Delete: " + Parcial.get().getStudentsDelete());
        System.out.println("EXP Count: " + Parcial.get().getExpToCount());
        System.out.println("EXP Delete: " + Parcial.get().getExpToDelete());
        for (Student student : students) {
            if (!student.getNames().contains(Parcial.get().getExpToDelete() + "")) {
                System.out.println(student.toString());
            }
        }
    }
}
