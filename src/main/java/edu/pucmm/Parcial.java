package edu.pucmm;

import edu.pucmm.domains.Student;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class Parcial {

    private static Parcial instance = null;

    private static final SecureRandom secureRandom = new SecureRandom();

    private static int SIZE = 25;
    private static int MIN_SIZE = 10;

    private char expToDelete;
    private char expToCount;

    private List<Student> students;
    private int studentsDelete = 0;
    private int studentsCount = 0;

    private static String[] Beginning = {"Kr", "Ca", "Ra", "Mrok", "Cru",
            "Ray", "Bre", "Zed", "Drak", "Mor", "Jag", "Mer", "Jar", "Mjol",
            "Zork", "Mad", "Cry", "Zur", "Creo", "Azak", "Azur", "Rei", "Cro",
            "Mar", "Luk"};
    private static String[] Middle = {"air", "ir", "mi", "sor", "mee", "clo",
            "red", "cra", "ark", "arc", "miri", "lori", "cres", "mur", "zer",
            "marac", "zoir", "slamar", "salmar", "urak"};
    private static String[] End = {"d", "ed", "ark", "arc", "es", "er", "der",
            "tron", "med", "ure", "zur", "cred", "mur"};

    private Parcial() {
        students = new ArrayList<>();
        List<Student> studentsCopy = new ArrayList<>();
        int cant = secureRandom.nextInt(SIZE);
        while (cant < MIN_SIZE) {
            cant = secureRandom.nextInt(SIZE);
        }
        for (int i = 0; i < cant; i++) {
            students.add(new Student((i + 1), generateName(), generateName()));
        }
        studentsCopy.addAll(students);
        expToCount = randomVocal();
        expToDelete = randomVocal();
        while (expToCount == expToDelete) {
            expToDelete = randomVocal();
        }
        for (Student student : students) {
            if (student.getNames().toLowerCase().contains(expToDelete + "")) {
                studentsDelete++;
                studentsCopy.remove(student);
            }
        }
        for (Student student : studentsCopy) {
            if (student.getNames().toLowerCase().contains(expToCount + "")) {
                studentsCount++;
            }
        }
        System.out.println("Inicio modelo");
    }

    public static Parcial get() {
        if (instance == null) {
            synchronized (Parcial.class) {
                if (instance == null) {
                    instance = new Parcial();
                }
            }
        }
        return instance;
    }

    public List<Student> getStudents() {
        return students;
    }

    public char getExpToDelete() {
        return expToDelete;
    }

    public char getExpToCount() {
        return expToCount;
    }

    public int getStudentsCount() {
        return studentsCount;
    }

    public int getStudentsDelete() {
        return studentsDelete;
    }

    private static char randomVocal() {
        switch (secureRandom.nextInt(5)) {
            case 0:
                return 'a';
            case 1:
                return 'e';
            case 2:
                return 'i';
            case 3:
                return 'o';
            case 4:
                return 'u';
        }
        return 'a';
    }

    private static String generateName() {
        return Beginning[secureRandom.nextInt(Beginning.length)] +
                Middle[secureRandom.nextInt(Middle.length)] +
                End[secureRandom.nextInt(End.length)];
    }
}
