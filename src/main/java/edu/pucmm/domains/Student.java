package edu.pucmm.domains;

import java.util.Objects;

public class Student {

    private Long id;
    private String names;
    private String lastnames;

    public Student() {
    }

    public Student(long id, String names, String lastnames) {
        this.id = id;
        this.names = names;
        this.lastnames = lastnames;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getLastnames() {
        return lastnames;
    }

    public void setLastnames(String lastnames) {
        this.lastnames = lastnames;
    }

    public Student copy() {
        return new Student(id, names, lastnames);
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o.getClass().equals(this.getClass()) && Objects.equals(this.id, ((Student) o).id);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "ID: " + id + " -- Estudiante: " + names + " " + lastnames;
    }
}
